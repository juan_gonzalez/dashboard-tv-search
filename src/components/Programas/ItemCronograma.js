import React from 'react';

const ItemCronograma = ({ serie }) => {
  const { name, season, number, _embedded } = serie;

  return (
    <div className='col-sm-6 col-md-4 col-lg-3 p-2 p-xl-3'>
      <div className="card border-0 text-white" style={{ height: '240px' }}>
        <img src={_embedded?.show?.image?.original} className="card-img border border-dark img-fluid" alt={name} style={{ height: '240px' }} />
        <div className="card-img-overlay d-flex flex-column justify-content-end">
          <div className='card-footer bg-light' style={{ opacity: '75%' }}>
            <h5 className="card-title text-dark">{_embedded?.show?.name}</h5>
            <div className='d-flex justify-content-between flex-wrap'>
              <p className="card-text text-dark">Temporada {season}</p>
              <p className="card-text text-dark">Episodio {number}</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ItemCronograma;
