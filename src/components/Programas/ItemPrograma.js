import React from 'react';
import { useHistory } from 'react-router-dom';

const ItemPrograma = ({ show }) => {
  const history = useHistory();

  // Método que redirecciona al usuario a ver los datalles de una serie en especifico
  const handleClickShow = () => {
    history.push(`/show/${show?.id}-${show?.name.replace(/ /gi, '-')}`);
  };

  return (
    <div className='col-sm-6 col-md-4 col-lg-3 p-2 p-xl-3'>
      <div 
        className="card border-0 text-white" 
        style={{ height: '385px', cursor: 'pointer' }}
        onClick={handleClickShow}
      >
        <img src={show?.image?.original} className="card-img border border-dark img-fluid" alt={show?.name} style={{ height: '385px' }} />
        <div className="card-img-overlay d-flex flex-column justify-content-end">
          <div className='card-footer bg-light' style={{ opacity: '75%' }}>
            <h5 className="card-title text-dark">{show?.name}</h5>
            <p className="card-text text-dark">{show?.genres.length > 1 ? 'Géneros:' : 'Género:'} {show?.genres.map((gen, idx, genres) => idx !== genres.length - 1 ? `${gen}, ` : gen)}</p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ItemPrograma;
