import React from 'react'

const Paginacion = ({ actualPage, pages, handleClickPageItem }) => (
  <nav aria-label="Page navigation">
    <ul className="pagination pagination-lg justify-content-evenly flex-wrap">
      <li className={`page-item ${actualPage - 1 === -1 && 'disabled'}`}>
        <button className='page-link' onClick={() => handleClickPageItem(actualPage - 1)}>
          <span aria-hidden="true">&laquo; Anterior</span>
        </button>
      </li>
      <li className="page-item disabled">
        <button className='page-link text-primary'>
          {actualPage + 1}
        </button>
      </li>
      <li className={`page-item ${actualPage + 1 === pages && 'disabled'}`}>
        <button className='page-link' onClick={() => handleClickPageItem(actualPage + 1)}>
          <span aria-hidden="true">Siguiente &raquo;</span>
        </button>
      </li>
    </ul>
  </nav>
);

export default Paginacion;
