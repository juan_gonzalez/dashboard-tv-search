import React, { useState, useContext } from 'react';
import { useHistory } from 'react-router-dom';
import authContext from '../../context/auth/authContext';

import Navbar from './Navbar';
import Sidebar from './Sidebar';

const Layout = (props) => {
  const { user } = useContext(authContext);

  // State que controla las vista del menú (Sidebar)
  const [toggle, setToggle] = useState(false);

  const history = useHistory();

  if (!user) history.push('/login');

  return (
    <div className={`d-flex p-0 m-0 ${toggle && 'toggled'}`} id="wrapper">
      <Sidebar />
    
      <div id="page-content-wrapper">
        <Navbar toggle={toggle} setToggle={setToggle} />

        <div className="container-fluid">
          {props.children}
        </div>
      </div>
    </div>
  );
};

export default Layout;
