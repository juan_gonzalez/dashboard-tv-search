import React from 'react';
import { Link, useLocation } from 'react-router-dom';

const SideBar = () => {
  const location = useLocation();

  return (
    <div className="bg-secondary" id="sidebar-wrapper">
      <div className="sidebar-heading text-light fw-bold text-center" style={{ fontSize: '22px' }}>TV Search</div>
      <div className="list-group list-group-flush">
        <Link 
          to='/'
          className={`list-group-item list-group-item-action bg-secondary text-light border-bottom ${location.pathname === '/' && 'border-primary'}`}
        >
          Inicio
        </Link>
        
        <Link
          to='/today-schedule' 
          className={`list-group-item list-group-item-action bg-secondary text-light border-bottom ${location.pathname === '/today-schedule' && 'border-primary'}`}
        >
          TV Hoy
        </Link>
      </div>
    </div>
  );
};

export default SideBar;
