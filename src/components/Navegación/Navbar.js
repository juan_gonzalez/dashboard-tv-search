import React, { useContext, useState } from 'react';
import { useHistory } from 'react-router-dom';
import Swal from 'sweetalert2';
import authContext from '../../context/auth/authContext';

import BarraBusqueda from './BarraBusqueda';

const Navbar = ({ toggle, setToggle }) => {
  const { user, userLogout } = useContext(authContext);
  const [search, setSearch] = useState('');

  const history = useHistory();

  /**
   * Método que permite cerrar la sesión actual del usuario
   */
  const handleClickLogout = () => {
    Swal.fire({
      text: "¿Deseas cerrar la sesión actual?",
      icon: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, deseo cerrarla'
    }).then((result) => {
      if (result.isConfirmed) {
        userLogout();
      }
    });
  };

  /**
   * Método que ejecuta la búsqueda realizada por el usuario
   * @param {*} e 
   */
  const handleSubmitSearch = e => {
    e.preventDefault();

    if (search.trim() === '') return;

    history.push(`/search/${search.replace(/ /gi, '-')}`);
  };

  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
      <div className="d-flex justify-content-between align-items-center w-100 mx-2">
        <div className='d-flex w-100'>
          <button 
            className="btn btn-outline-secondary me-3" 
            id="menu-toggle"
            onClick={() => setToggle(!toggle)}
          >
            <span className="navbar-toggler-icon"></span>
          </button>

          <BarraBusqueda
            search={search}
            setSearch={setSearch}  
            handleSubmitSearch={handleSubmitSearch} 
          />
        </div>

        {user && 
          <div className="btn-group me-2">
            <button type="button" className="btn btn-link dropdown-toggle dropdown-toggle-split d-flex justify-content-center align-items-center" data-bs-toggle="dropdown" aria-expanded="false">
              <img src={user?.picture?.medium} alt={`${user?.uuid}`} className='rounded-circle me-2' style={{ width: '50px' }} />
              <span className="visually-hidden">Toggle Dropdown</span>
            </button>

            <ul className="dropdown-menu dropdown-menu-end">
              <li>
                <button 
                  className='btn btn-link dropdown-item'
                  onClick={handleClickLogout}
                >
                  <div className='d-flex align-items-center'>
                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" className="bi bi-box-arrow-right align-middle me-2 text-danger" viewBox="0 0 16 16">
                      <path fillRule="evenodd" d="M10 12.5a.5.5 0 0 1-.5.5h-8a.5.5 0 0 1-.5-.5v-9a.5.5 0 0 1 .5-.5h8a.5.5 0 0 1 .5.5v2a.5.5 0 0 0 1 0v-2A1.5 1.5 0 0 0 9.5 2h-8A1.5 1.5 0 0 0 0 3.5v9A1.5 1.5 0 0 0 1.5 14h8a1.5 1.5 0 0 0 1.5-1.5v-2a.5.5 0 0 0-1 0v2z"/>
                      <path fillRule="evenodd" d="M15.854 8.354a.5.5 0 0 0 0-.708l-3-3a.5.5 0 0 0-.708.708L14.293 7.5H5.5a.5.5 0 0 0 0 1h8.793l-2.147 2.146a.5.5 0 0 0 .708.708l3-3z"/>
                    </svg>
                    Cerrar Sesión
                  </div>
                </button>
              </li>
            </ul>
          </div>
        }
      </div>
    </nav>
  );
};

export default Navbar;
