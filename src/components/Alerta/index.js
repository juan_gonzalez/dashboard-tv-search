import React from 'react';

const Alerta = ({ message, styles = { width:'100%' }}) => (
  <div className={`alert alert-${message.type} text-center`} style={styles} role="alert">
    {message.msg}
  </div>
);

export default Alerta;
