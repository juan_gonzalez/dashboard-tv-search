import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import AuthState from './context/auth/authState';
import TVState from './context/tv/tvState';

import Main from './pages/Main';
import InicioSesion from './pages/InicioSesion';
import Busqueda from './pages/Busqueda';
import CronogramaHoy from './pages/CronogramaHoy';
import InfoPrograma from './pages/InfoPrograma';

const App = () => (
  <AuthState>
    <TVState>
      <Router>
        <Switch>
          <Route exact path='/' component={Main} />
          <Route exact path='/login' component={InicioSesion} />
          <Route exact path='/search/:slug' component={Busqueda} />
          <Route exact path='/show/:slug' component={InfoPrograma} />
          <Route exact path='/today-schedule' component={CronogramaHoy} />
        </Switch>
      </Router>
    </TVState>
  </AuthState>
);

export default App;
