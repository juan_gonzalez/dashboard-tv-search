import React, { useReducer } from 'react';
import TVContext from './tvContext';
import tvReducer from './tvReducer';

import {
  OBTENER_SHOWS_EXITO,
  OBTENER_SHOWS_ERROR,
  OBTENER_PAGINACION_EXITO,
  OBTENER_PAGINACION_ERROR,
  BUSQUEDA_SHOWS_EXITO,
  SELECCIONAR_SHOW,
  LIMPIAR_SHOW_SELECCIONADO,
  LIMPIAR_BUSQUEDA,
  LIMPIAR_SHOWS,
  OCULTAR_MENSAJE
} from '../types';

const baseUrl = 'http://api.tvmaze.com';

const TVState = (props) => {
  const initialState = {
    selectedShow: null,
    shows: [],
    search: [],
    pages: 0,
    message: null
  };

  const [state, dispatch] = useReducer(tvReducer, initialState);

  // Funciones que modifican el Reducer (Actions)
  /**
   * Método que obtiene los shows de tv de la api rest
   * @param {*} page 
   */
  const getShowsTV = async (page) => {
    try {
      const result = await fetch(`${baseUrl}/shows?page=${page}`, {
        method: 'get'
      });
      const response = await result.json();

      dispatch({
        type: OBTENER_SHOWS_EXITO,
        payload: response
      });

    } catch (error) {
      dispatch({
        type: OBTENER_SHOWS_ERROR,
        payload: { msg: 'Hubo un error al obtener los shows de televisión', type: 'danger' }
      });
    }

    setTimeout(() => {
      hideMessage();
    }, 3000);
  };

  /**
   * Método que obtiene la cantidad total de shows de la api rest tv maze para la realización de la paginación
   */
  const getTotalPages = async () => {
    try {
      const result = await fetch(`${baseUrl}/updates/shows`, {
        method: 'get'
      });
      const response = await result.json();

      dispatch({
        type: OBTENER_PAGINACION_EXITO,
        payload: Math.round(Object.keys(response).length / 250)
      });

    } catch (error) {
      dispatch({
        type: OBTENER_PAGINACION_ERROR,
        payload: { msg: 'Hubo un error al obtener la paginación total de la api', type: 'danger' }
      });
    }

    setTimeout(() => {
      hideMessage();
    }, 3000);
  };

  /**
   * Método que obtiene los resultados de la búsqueda realizada por el usuario
   * @param {*} search 
   * @param {*} type 
   */
  const searchTVShows = async (search, type) => {
    try {
      let searchUrl;

      switch (type) {
        case 'SearchBar':
          searchUrl = `${baseUrl}/search/shows?q=${search}`;
          break;

        case 'Schedule':
          searchUrl = `${baseUrl}/schedule/web?date=${search}`;
          break;
      
        default:
          searchUrl = null;
          break;
      }

      const result = await fetch(searchUrl, {
        method:'get'
      });
      const response = await result.json();

      dispatch({
        type: BUSQUEDA_SHOWS_EXITO,
        payload: response
      });
    } catch (error) {
      dispatch({
        type: OBTENER_SHOWS_ERROR,
        payload: { msg: 'Hubo un error al obtener los shows de televisión', type: 'danger' }
      });
    }

    setTimeout(() => {
      hideMessage();
    }, 3000);
  };

  /**
   * Método que obtiene un show determinado deacuerdo a su id
   * @param {*} idShow 
   */
  const selectShow = async (idShow) =>  {
    try {
      const result = await fetch(`${baseUrl}/shows/${idShow}?embed=cast`, {
        method: 'get'
      });
      const response = await result.json();

      dispatch({
        type: SELECCIONAR_SHOW,
        payload: response
      });
    } catch (error) {
      dispatch({
        type: OBTENER_SHOWS_ERROR,
        payload: { msg: 'Hubo un error al obtener el show de televisión', type: 'danger' }
      });
    }

    setTimeout(() => {
      hideMessage();
    }, 3000);
  };

  /**
   * Método que limpia el atributo shows del Reducer
   */
  const clearTVShows = () => {
    dispatch({ type: LIMPIAR_SHOWS });
  };

  /**
   * Método que limpia el atributo search del Reducer
   */
  const clearSearch = () => {
    dispatch({ type: LIMPIAR_BUSQUEDA });
  };

  /**
   * Método que limpia el atributo selectedSearch del Reducer
   */
  const clearSelectedShow = () => {
    dispatch({ type: LIMPIAR_SHOW_SELECCIONADO });
  };

  /**
   * Método que limpia los mensajes de error al realizar una consulta con la api de tv maze
   */
  const hideMessage = () => {
    dispatch({ type: OCULTAR_MENSAJE });
  };

  return (
    <TVContext.Provider 
      value={{
        shows: state.shows,
        search: state.search,
        selectedShow: state.selectedShow,
        pages: state.pages,
        message: state.message,
        getShowsTV,
        searchTVShows,
        selectShow,
        getTotalPages,
        clearTVShows,
        clearSearch,
        clearSelectedShow
      }}
    >
      {props.children}
    </TVContext.Provider>
  );
};

export default TVState;
