import {
  OBTENER_SHOWS_EXITO,
  OBTENER_SHOWS_ERROR,
  OBTENER_PAGINACION_EXITO,
  OBTENER_PAGINACION_ERROR,
  BUSQUEDA_SHOWS_EXITO,
  SELECCIONAR_SHOW,
  LIMPIAR_SHOW_SELECCIONADO,
  LIMPIAR_BUSQUEDA,
  LIMPIAR_SHOWS,
  OCULTAR_MENSAJE
} from '../types';

/*
  Atributos de tvState
  { selectedShow, shows, search, pages, message }
*/
const tvReducer = (state, action) => {
  switch (action.type) {
    case OBTENER_SHOWS_EXITO:
      return {
        ...state,
        shows: action.payload
      };

    case BUSQUEDA_SHOWS_EXITO:
      return {
        ...state,
        search: action.payload
      };

    case OBTENER_PAGINACION_EXITO:
      return {
        ...state,
        pages: action.payload
      };

    case OBTENER_PAGINACION_ERROR:
    case OBTENER_SHOWS_ERROR:
      return {
        ...state,
        message: action.payload
      };

    case SELECCIONAR_SHOW:
      return {
        ...state,
        selectedShow: action.payload
      };

    case LIMPIAR_SHOWS:
      return {
        ...state,
        shows: [],
        pages: 0
      };

    case LIMPIAR_BUSQUEDA:
      return {
        ...state,
        search: []
      };

    case LIMPIAR_SHOW_SELECCIONADO:
      return {
        ...state,
        selectedShow: null
      };

    case OCULTAR_MENSAJE:
      return {
        ...state,
        message: null
      };
  
    default:
      return state;
  }
};

export default tvReducer
