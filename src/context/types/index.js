// Types para la gestión del inicio de sesión
export const INICIAR_SESION_EXITO = 'INICIAR_SESION_EXITO';
export const INICIAR_SESION_ERROR = 'INICIAR_SESION_ERROR';
export const CERRAR_SESION_USUARIO = 'CERRAR_SESION_USUARIO';

// Types para la gestión de los shows de TV Maze
export const OBTENER_SHOWS_EXITO = 'OBTENER_SHOWS_EXITO';
export const BUSQUEDA_SHOWS_EXITO = 'BUSQUEDA_SHOWS_EXITO';
export const SELECCIONAR_SHOW = 'SELECCIONAR_SHOW';
export const OBTENER_SHOWS_ERROR = 'OBTENER_SHOWS_ERROR';
export const OBTENER_PAGINACION_EXITO = 'OBTENER_PAGINACION_EXITO';
export const OBTENER_PAGINACION_ERROR = 'OBTENER_PAGINACION_ERROR';
export const LIMPIAR_SHOWS = 'LIMPIAR_SHOWS';
export const LIMPIAR_SHOW_SELECCIONADO = 'LIMPIAR_SHOW_SELECCIONADO';
export const LIMPIAR_BUSQUEDA = 'LIMPIAR_BUSQUEDA';

// Type para la gestión de los mensajes de la aplicación
export const OCULTAR_MENSAJE = 'OCULTAR_MENSAJE';