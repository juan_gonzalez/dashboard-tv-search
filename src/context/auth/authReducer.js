import {
  INICIAR_SESION_EXITO,
  INICIAR_SESION_ERROR,
  CERRAR_SESION_USUARIO,
  OCULTAR_MENSAJE
} from '../types';

/*
  Atributos de authState
  { user, message }
*/
const authReducer = (state, action) => {
  switch (action.type) {
    case INICIAR_SESION_EXITO:
      localStorage.setItem('user', JSON.stringify(action.payload));
      return {
        ...state,
        user: action.payload
      };

    case INICIAR_SESION_ERROR:
      return {
        ...state,
        user: null,
        message: action.payload
      };

    case CERRAR_SESION_USUARIO:
      localStorage.removeItem('user');
      return {
        ...state,
        user: null
      };

    case OCULTAR_MENSAJE:
      return {
        ...state,
        message: null
      };
  
    default:
      return state;
  }
}

export default authReducer;
