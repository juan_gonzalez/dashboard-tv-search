import React, { useReducer } from 'react';
import AuthContext from './authContext';
import authReducer from './authReducer';

import {
  INICIAR_SESION_EXITO,
  INICIAR_SESION_ERROR,
  CERRAR_SESION_USUARIO,
  OCULTAR_MENSAJE
} from '../types';

const AuthState = (props) => {
  const initialState = {
    user: JSON.parse(localStorage.getItem('user')),
    message: null
  };

  const [state, dispatch] = useReducer(authReducer, initialState);

  // Métodos que modifican el State del reducer (Actions)
  /**
   * Método que válida los credenciales digitados por el usuario para iniciar sesión
   * @param {*} account 
   */
  const userLogin = async (account) => {
    const result = await fetch('http://localhost:4000/usuarios', {
      method:'get'
    });
    const response = await result.json();
    const userAccount = response.filter(user => user.username === account.username || user.email === account.username)[0];

    if (userAccount) {
      if (userAccount.password === account.password) {
        dispatch({
          type: INICIAR_SESION_EXITO,
          payload: userAccount
        });
      } else {
        dispatch({
          type: INICIAR_SESION_ERROR,
          payload: { msg: 'El password digitado es incorrecto', type: 'danger' }
        });
      }
    } else {
      dispatch({
        type: INICIAR_SESION_ERROR,
        payload: { msg: 'El usuario digitado no existe', type: 'danger' }
      });
    }

    setTimeout(() => {
      hideMessage()
    }, 3000);
  };

  /**
   * Método que permite cerrar la sesión actual de un usuario
   */
  const userLogout = () => {
    dispatch({ type: CERRAR_SESION_USUARIO });
  };

  /**
   * Método que limpia los mensajes de error al iniciar sesión
   */
  const hideMessage = () => {
    dispatch({ type: OCULTAR_MENSAJE });
  };

  return (
    <AuthContext.Provider
      value={{
        user: state.user,
        message: state.message,
        userLogin,
        userLogout
      }}
    >
      {props.children}
    </AuthContext.Provider>
  );
};

export default AuthState;
