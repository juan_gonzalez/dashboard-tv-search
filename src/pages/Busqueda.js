import React, { useEffect, useContext } from 'react';
import { useParams } from 'react-router-dom';
import tvContext from '../context/tv/tvContext';
import authContext from '../context/auth/authContext';

import Layout from '../components/Navegación';
import ItemPrograma from '../components/Programas/ItemPrograma';

const Busqueda = () => {
  const { search, searchTVShows, clearSearch } = useContext(tvContext);
  const { user } = useContext(authContext);

  let { slug } = useParams();

  // Obtiene los resultados obtenidos de la búsqueda realizada
  useEffect(() => {
    if (user) searchTVShows(slug, 'SearchBar');

    return () => clearSearch(); // eslint-disable-next-line
  }, []);

  return (
    <Layout>
      <div className='text-center my-4'>
        <h1>Búsqueda por "{slug}"</h1>
        <p>{search.length} resultados encontrados</p>
      </div>

      <div className='row mb-4'>
        {search.map((show, idx) => (
         <ItemPrograma key={idx} show={show?.show} />
        ))}
      </div>
    </Layout>
  )
}

export default Busqueda;
