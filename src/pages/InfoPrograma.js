import React, { useContext, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import tvContext from '../context/tv/tvContext';
import authContext from '../context/auth/authContext';

import Layout from '../components/Navegación';

const InfoPrograma = () => {
  const { selectedShow, selectShow, clearSelectedShow } = useContext(tvContext);
  const { user } = useContext(authContext);

  let { slug } = useParams();

  // Obtiene la información del show seleccionado
  useEffect(() => {
    if (user) selectShow(slug.split('-')[0]);

    return () => clearSelectedShow(); // eslint-disable-next-line
  }, []);

  return (
    <Layout>
      <div className="card my-4 p-0">
        <img src={selectedShow?.image?.original} alt={slug} className='card-img-top' style={{width: '100%', height: '600px' }} />
        <div className="card-body">
          <p className="card-title text-center" style={{ fontSize: '80px' }} >{selectedShow?.name}</p>
          <div className='mb-3'>
            <p className='card-subtitle fw-bold fs-4'>Resumen:</p>
            <p className="card-text">{selectedShow?.summary}</p>
          </div>

          <div className='d-flex flex-wrap justify-content-between mb-4'>
            <div>
              <p className='card-subtitle fw-bold fs-4'>{selectedShow?.genres.length > 1 ? 'Géneros' : 'Género'}</p>
              <p className="card-text text-center">{selectedShow?.genres.map((gen, idx, genres) => idx === genres.length - 1 ? gen : `${gen}, `)}</p>
            </div>

            <div>
              <p className='card-subtitle fw-bold fs-4'>Estado</p>
              <p className="card-text text-center">{selectedShow?.status}</p>
            </div>

            <div>
              <p className='card-subtitle fw-bold fs-4'>Valoración</p>
              <div className='d-flex justify-content-center align-items-center'>
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-star-fill" viewBox="0 0 16 16">
                  <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.283.95l-3.523 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"/>
                </svg>
                <p className="card-text ms-2">{selectedShow?.rating?.average}</p>
              </div>
            </div>
          </div>
          
          <div className='mb-4'>
            <p className='card-subtitle fw-bold fs-4'>Elenco</p>
            <p className="card-text">{selectedShow?._embedded?.cast.map((c, idx, cast) => idx === cast.length - 1 ? c?.person?.name : `${c?.person?.name}, `)}</p>
          </div>

          <div className='d-flex justify-content-center'>
            <a 
              className={`btn btn-primary w-25 text-uppercase fw-bold ${!selectedShow?.officialSite && 'disabled'}`} 
              href={selectedShow?.officialSite} 
              target='blank'
            >
              Sitio Oficial
            </a>
          </div>
        </div>
      </div>
    </Layout>
  );
};

export default InfoPrograma;
