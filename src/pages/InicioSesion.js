import React, { useState, useContext, useEffect } from 'react';
import authContext from '../context/auth/authContext';

import FormularioIngreso from '../components/InicioSesion/FormularioIngreso';
import Alerta from '../components/Alerta';

const InicioSesion = (props) => {
  const { user, message, userLogin } = useContext(authContext);

  const [account, setAccount] = useState({
    username: '',
    password: ''
  });

  // Válida si el usuario está autenticado y lo redirecciona a la página principal
  useEffect(() => {
    if (user) props.history.push('/');
    // eslint-disable-next-line
  }, [user]);

  /**
   * Método que captura los datos del formulario de inicio de sesión y actualiza el state del componente
   * @param {*} e 
   */
  const handleChangeForm = e => {
    setAccount({
      ...account,
      [e.target.name]: e.target.value
    });
  };

  /**
   * Método que permite iniciar sesión a un usuario en la aplicación
   * @param {*} e 
   */
  const handleSubmitLogin = e => {
    e.preventDefault();
    const { username, password } = account;

    if (username.trim() === '' || password.trim() === '') return;

    userLogin(account);
  };

  return (
    <div className='d-flex flex-column justify-content-center align-items-center min-vh-100 bg-secondary bg-gradient'>
      {message && (
        <Alerta  message={message} styles={{ width: '500px' }} />
      )}

      <FormularioIngreso 
        account={account}
        handleChangeForm={handleChangeForm}
        handleSubmitLogin={handleSubmitLogin}
      />
    </div>
  );
};

export default InicioSesion;
