import React, { useEffect, useContext, useState } from 'react';
import tvContext from '../context/tv/tvContext';
import authContext from '../context/auth/authContext';

import Layout from '../components/Navegación';
import Paginacion from '../components/Navegación/Paginacion';
import ItemPrograma from '../components/Programas/ItemPrograma';

const Main = () => {
  const { shows, pages, getShowsTV, getTotalPages, clearTVShows } = useContext(tvContext);
  const { user } = useContext(authContext);

  const [actualPage, setActualPage] = useState(0);

  // Obtiene la cantidad total de páginas para la paginación del componente
  useEffect(() => {
    if (user) getTotalPages();
    return () => clearTVShows(); // eslint-disable-next-line
  }, []);

  // Válida los cambios de la página actual y actualiza los shows del reducer
  useEffect(() => {
    if (user) getShowsTV(actualPage); // eslint-disable-next-line
  }, [actualPage]);

  // Método que actualiza la página actual del componente
  const handleClickPageItem = (page) => {
    setActualPage(page);
    window.scrollTo(0, 0);
  };

  return (
    <Layout>
      <div className='row mb-4'>
        {shows.map(show => (
         <ItemPrograma key={show?.id} show={show} />
        ))}
      </div>
      
      <Paginacion 
        actualPage={actualPage}
        pages={pages}
        handleClickPageItem={handleClickPageItem}
      />
    </Layout>
  );
};

export default Main;
