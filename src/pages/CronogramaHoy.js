import React, { useEffect, useContext } from 'react';
import tvContext from '../context/tv/tvContext';
import authContext from '../context/auth/authContext';

import Layout from '../components/Navegación';
import ItemCronograma from '../components/Programas/ItemCronograma';

const CronogramaHoy = () => {
  const { search, searchTVShows, clearSearch } = useContext(tvContext);
  const { user } = useContext(authContext);

  // Obtiene los resultados obtenidos de los shows que se transmiten el día de hoy
  useEffect(() => {
    if (user) {
      const date = new Date();
      searchTVShows(date.toJSON().slice(0, 10), 'Schedule');
    }

    return () => clearSearch(); // eslint-disable-next-line
  }, []);

  return (
    <Layout>
      <div className='text-center my-4'>
        <h1>Cronograma de Hoy</h1>
      </div>

      <div className='row mb-4'>
        {search.map((show, idx) => (
          <ItemCronograma key={idx} serie={show} />
        ))}
      </div>
    </Layout>
  )
}

export default CronogramaHoy;
